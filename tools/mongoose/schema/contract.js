var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var contractSchema = new Schema(
    {
        teacherID: ObjectId, // User ID
        studentID: ObjectId,
        state: String,
        title: String,
        description: String,
        feeHourly: String,
        timeStudy: Number,
        skill: ObjectId,
        finishAt: String,
        feedback: {
            content: String,
            rate: Number,
            createAt: String
        }
    },
    { versionKey: false }
);

module.exports = mongoose.model('Contract', contractSchema);
