var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var complaintSchema = new Schema(
    {
        contractID: ObjectId,
        studentMessage: String,
        teacherMessage: String,
        state: String,
        whoAccepted: String,
        createAt: String
    },
    { versionKey: false }
);

module.exports = mongoose.model('Complaint', complaintSchema);
