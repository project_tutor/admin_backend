var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var postSchema = new Schema(
    {
        teacherID: ObjectId, // User ID
        title: String,
        overview: String,
        hourlyRate: String,
        skill: [ObjectId], // Tag ID
        education: [
            {
                name: String,
                timeStart: String,
                timeEnd: String,
                degree: String,
                description: String
            }
        ],
        employeeHistory: [
            {
                title: String,
                workPlace: String,
                timeStart: String,
                timeEnd: String,
                description: String
            }
        ],
        isVisable: Boolean,
        rate: [Number],
        students: [ObjectId] // User ID
    },
    { versionKey: false }
);

module.exports = mongoose.model('Post', postSchema);
