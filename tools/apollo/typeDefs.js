const { gql } = require('apollo-server-express');

module.exports = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Book" type defines the queryable fields for every book in our data source.
  type User {
    _id: ID
    fullname: String
    email: String
    imageURL: String
    location: Location
    phone: String
    role: String
    isActived: Boolean
    isReadEmail: Boolean
    isLocked: Boolean
    money:String
  }

  type Location {
    address: String
    city: String
  }

  type Token {
    token: String
    user: User
  }


  input UserInputAdd {
    fullname: String
    email: String
    password: String
    role: Role
   
  }

  input UserInputEdit {
    fullname: String
    role: Role
    isLocked: Boolean
    money:String
  }

  input LoginInput {
    email: String
    password: String
  }

  enum Role {
    STUDENT
    TEACHER
    ADMIN
  }

  # Tag
  type Tag{
    _id: ID
    name: String,
    isAproved: Boolean
  }

  input TagInput {
    name: String,
    isAproved: Boolean
  }

  # Contract
  type Contract{
    _id: ID
    teacher: User,
    student: User,
    state: STATE_CONTRACT,
    title: String,
    description: String,
    feeHourly: String,
    timeStudy: Int,
    skill: Tag,
    finishAt: String,
  }
  
  enum STATE_CONTRACT {
    PENDING
    WAITINGPAY
    AGREE
    DISAGREE
    FINISH
    COMPLAINING
    STUDENTWIN 
    TEACHERWIN
  }

   # Complaint
   type Complaint{
    _id: ID,
    contract: Contract,
    studentMessage: String,
    teacherMessage: String,
    state: STATE_COMPLAINT,
    whoAccepted: WHOACCEPTED_COMPLAINT,
    createAt: String
  }

  enum STATE_COMPLAINT {
    PENDING
    PROCESSED
  }

  enum WHOACCEPTED_COMPLAINT {
    STUDENT
    TEACHER
  }

  input  CONTRACT_INPUT{
    state:STATE_CONTRACT,
    finishAt:String
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query {
    # User
    users: [User]
    userLogin: User

    # Tag
    tags: [Tag]

    # Contract
    contracts: [Contract]

    # Complaint
    complaints: [Complaint]
  }

  type Mutation {
    # User
    addUser(input: UserInputAdd!): Boolean
    editUser(userID: ID!, input: UserInputEdit!): Boolean
    deleteUser(userID: ID!): Boolean
    #contract
    updateContract(contractid:ID!,input:CONTRACT_INPUT!): Boolean
    # Login
    login(input: LoginInput!): Token
    #complaint
    updateComplaint(complaintId:ID!,whoAccepted:WHOACCEPTED_COMPLAINT!):Boolean
    # Tag
    addTag(input: TagInput!): Boolean
    editTag(tagID: ID!, input: TagInput!): Boolean
    deleteTag(tagID: ID!): Boolean
  }

  type Subscription {
    subUsers: User
  }
`;
