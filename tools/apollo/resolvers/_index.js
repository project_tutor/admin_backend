const merge = require("lodash/merge");

const user = require("./user");
const login = require("./login");
const tag = require("./tag");
const contract = require("./contract");
const complaint = require("./complaint");

module.exports = merge(user, login, tag, contract, complaint);
