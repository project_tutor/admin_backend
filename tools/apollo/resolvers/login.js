const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const userSchema = require('@schema/user');

module.exports = {
  Query: {
    userLogin: async (parent, args, context, info) => {
      if (!context.user) {
        return null;
      }
      return await userSchema.findOne({ username: context.user.username });
    }
  },

  Mutation: {
    login: async (parent, args, context, info) => {
      const { email, password } = args.input;
      const user = await userSchema.findOne({
        email
      });
      if (!user) {
        throw new Error('Email không chính xác!');
      } else {
        //Check password
        const checkPassword = await bcrypt.compare(password, user.password);
        if (!checkPassword) {
          throw new Error('Mật khẩu không chính xác!');
        }
        if (user.isActived === false) {
          throw new Error('Chưa xác nhận email!');
        }
      }

      if (user.role !== "ADMIN") {
        throw new Error('Admin mới có quyền truy cập!');
      }

      const token = jwt.sign(
        {
          email: user.email
        },
        'your_jwt_secret'
      );
      return {
        token,
        user
      };
    }
  }
};
