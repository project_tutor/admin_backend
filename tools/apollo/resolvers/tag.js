const tagSchema = require('@schema/tag');

module.exports = {
  Query: {
    tags: async (parent, args, context, info) => {
      return await tagSchema.find();
    }
  },
  Mutation: {
    addTag: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { input } = args;
      await new tagSchema(input).save();
      return true;
    },
    editTag: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { tagID, input } = args;
      // await new tagSchema({ name, isAproved: false }).save();
      await tagSchema.updateOne({ _id: tagID }, input);
      return true;
    },
    deleteTag: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { tagID } = args;
      await tagSchema.deleteOne({ _id: tagID });
      return true;
    }
  }
};
