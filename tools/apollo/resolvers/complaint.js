const contractSchema = require('@schema/contract');
const complaintSchema = require('@schema/complaint');
const userSchema = require('@schema/user');

module.exports = {
  Query: {
    complaints: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const contracts = await contractSchema.find({
        state: ['COMPLAINING', 'STUDENTWIN', 'TEACHERWIN']
      });

      const arrayContractID = contracts.map((item) => item._id);

      const complaints = await complaintSchema.find({
        contractID: arrayContractID
      });

      const complaintsReturn = [];
      for (let i = 0; i < complaints.length; i++) {
        const theContract = await contractSchema.findOne({ _id: complaints[i].contractID });
        theContract.student = await userSchema.findOne({ _id: theContract.studentID });
        theContract.teacher = await userSchema.findOne({ _id: theContract.teacherID });
        complaintsReturn.push({
          _id: complaints[i]._id,
          studentMessage: complaints[i].studentMessage,
          teacherMessage: complaints[i].teacherMessage,
          state: complaints[i].state,
          whoAccepted: complaints[i].whoAccepted,
          createAt: complaints[i].createAt,
          contract: theContract
        });
      }

      return complaintsReturn;
    },
  },
  Mutation: {
    updateComplaint: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { complaintId, whoAccepted } = args;
      await complaintSchema.updateOne({ _id: complaintId }, { whoAccepted, state: "PROCESSED" });
      return true;
    },
  },
};
