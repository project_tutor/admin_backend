const { PubSub, AuthenticationError } = require('apollo-server-express');
const bcrypt = require('bcrypt');
const pubsub = new PubSub();

const userSchema = require('@schema/user');

module.exports = {
  Query: {
    users: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const user = await userSchema.findOne({ email: context.user.email });

      return await userSchema.find({ _id: { $ne: user._id } });
    }
  },

  Mutation: {
    addUser: async (parent, args, context, info) => {
      const { fullname, email, password, role } = args.input;

      //check user đã tồn tại
      const listUsers = await userSchema.find({ email: email });
      if (listUsers.length > 0) {
        throw Error('Email đã tồn tại!');
      }

      //hash password
      const passwordHashed = await bcrypt.hash(
        password,
        +process.env.SALT_ROUNDS
      );

      //Save new user vao database
      const newUser = await new userSchema({
        fullname,
        email,
        password: passwordHashed,
        role,
        isActived: true,
        isReadEmail: false,
        isLocked: false
      }).save();

      if (newUser) {
        return true;
      } else {
        throw Error('Tạo User thất bại!');
      }

    },
    editUser: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { userID, input } = args;
      await userSchema.updateOne({ _id: userID }, input)
      return true;
    },
    deleteUser: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { userID } = args;
      await userSchema.deleteOne({ _id: userID })
      return true;
    }
  },

  Subscription: {
    subUsers: {
      subscribe: (parent, args, context, info) => {
        return pubsub.asyncIterator(['USERS']);
      }
    }
  }
};
