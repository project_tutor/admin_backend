const userSchema = require('@schema/user');
const contractSchema = require('@schema/contract');
const tagSchema = require('@schema/tag');

module.exports = {
  Query: {
    contracts: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const arrContractReturn = [];
      const contract = await contractSchema.find({});
      for (let i = 0; i < contract.length; i++) {
        // Lấy teacher student 
        const teacher = await userSchema.findOne({ _id: contract[i].teacherID });
        const student = await userSchema.findOne({ _id: contract[i].studentID });
        // Lây skill
        const skill = await tagSchema.findOne({ _id: contract[i].skill });
        arrContractReturn.push({
          _id: contract[i]._id,
          teacher,
          student,
          state: contract[i].state,
          title: contract[i].title,
          description: contract[i].description,
          feeHourly: contract[i].feeHourly,
          timeStudy: contract[i].timeStudy,
          finishAt: contract[i].finishAt,
          skill
        });
      }
      return arrContractReturn;
    },
  },
  Mutation: {
    updateContract: async (parent, args, context, info) => {
      if (!context.user) {
        throw new AuthenticationError('Authentication token is invalid');
      }
      const { contractid, input } = args;
      await contractSchema.updateOne({ _id: contractid }, input);
      return true;
    },
  }
};
