const moduleAlias = require("module-alias");

module.exports = () => {
    moduleAlias.addAliases({
        "@root": __dirname + "/..",
        "@public": __dirname + "/.." + "/public",
        "@config": __dirname + "/.." + "/config",
        "@tools": __dirname + "/.." + "/tools",
        "@schema": __dirname + "/.." + "/tools/mongoose/schema"
    });
};
